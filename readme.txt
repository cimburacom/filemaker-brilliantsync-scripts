Setup Instructions:
1) Copy the table BrilliantSyncSettings and paste it into your solution
2) Copy/Paste or import the BrilliantSync scripts folder into your solution
3) Create a script or button that calls "BrilliantSync Pull" with the name of the layout for the ESS table you wish to sync as the script parameter




-- Provided script
function initialize_bsync_timestamps() {
	-- Check if bsync_settings table exists. 
	-- If it does, return immediately. 

	-- If it doesn't, export a CSV file, then do an import to new table to make it exist.
}


-- Provided script
-- Provided by LF 
function bsync_now( layoutname, callbackfunc) {
	initialize_bsync_timestamps()


	-- Get myLastSyncTime from settings, for tablename
	let myLastSyncTime = GetFirstRecord( From bsync_settings WHERE layoutname=$layoutname )

	if ( myLastSyncTime is empty ) {
		myLastSyncTime = '1969-01-01 00:00:00'
		INSERT RECORD INTO bsync_settings ( layoutname=$layoutname, sync_time=$myLastSyncTime )
	}

	-- Query all records where timestamp is greater than stored timestamp
	resultsSet = SELECT * FROM $tablename WHERE xUpdatedTS > $myLastSyncTime

	-- Temp storage for last seen timestamp
	let myTempLastTime = myLastSyncTime

	let recordsSynced = 0;
	-- Loop through results and process them.
	for each record in resultSet: 
		
		-- IDK how to do it, but store it in FileMaker.
		callbackfunc( record );

		if ( record.xUpdatedTS > myTempLastTime ) {
			myLastSyncTime = myTempLastTime
			myTempLastTime = record.xUpdatedTS
		}

		recordsSynced++

		if ( recordsSynced % 100 === 0 ) {
		-- You may want to periodically store myLastSyncTime back into your settings in case 
		-- sync is interrupted.
			UPDATE bsync_settings SET sync_time=$myLastSyncTime WHERE layoutname=$layoutname
		}

	-- If no more records, then we can set the myLastSyncTime to the final myTempLastTime
	UPDATE bsync_settings SET sync_time=$myLastSyncTime WHERE layoutname=$layoutname
}


// Sample provided by LF, but probably replaced/customized by client
function my_callbackfunc( record ) {
	// do whatever they want with each record
	copy ODBC record into FM record
}

// Client schedules script step
bsync_now( 'contacts_layout', 'my_callbackfunc');